int pin = 13;
volatile int delayus = 0; // microseconds

void setup()
{
  pinMode(2, INPUT);
  pinMode(pin, OUTPUT);
  
  digitalWrite(2, HIGH);
  digitalWrite(pin, LOW);
  
  attachInterrupt(0, blink, FALLING);
}

void loop()
{
  //digitalWrite(pin, state);
  
}

void blink()
{
  noInterrupts();

  delayMicroseconds(delayus);
  
  PORTB = 1 << 5;
  delayMicroseconds(1000);
  PORTB = 0;
  
  delayMicroseconds(10000);
  delayMicroseconds(10000);
  delayMicroseconds(10000);
  delayMicroseconds(10000);
  
  interrupts();
}
